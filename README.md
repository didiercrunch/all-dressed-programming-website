# All-Dressed Programming 

## Dev

### Technologies

* [Hugo](https://gohugo.io/)
* [Loveit](https://hugoloveit.com/theme-documentation-basics/) hugo team
* [nix flake](https://nixos.wiki/wiki/Flakes)


### Aide mémoire

#### Run development server

```shell
hugo serve --disableFastRender --buildDrafts
```

#### Deploy

```shell
deployctl deploy --prod -p all-dressed-programming  server.ts
```

##### URL
https://all-dressed-programming-zwec4dgjpjs0.deno.dev