---
title: "Personal Introduction To C#"
date: 2023-05-24T12:04:20-04:00
draft: false
---

## Background

I have never learned C# for two main reasons.  First, as C# is a direct
competitor to [java][java], I have always favoured to use a technology I already
master over a new technology.  Second, I am a member of the Microsoft Haters Social
Club; for almost theological reasons, I avoid anything that is remotly close to 
the MS world.  Consequently, C# was label "I know it exists" by my brain for many
years.

But now, I have 2 separate reasons to learn C#.  First, I would like to play with
a game engine and most of them are using C# as scripting language.  Second, I might
teach a programming class that uses C# as main programming language


## Prejudices


My main prejudice about C# is that it is a copy of java.  I believe the language
has been developped by the same person as typescript (which is amazing).  I know
stackoverflow is written with it and runs on an incredibly low number of servers. 
I believe Rich Hickey said that C# async mechanism has been more inspiring then Go
when he wrote clojure's core async library.  C# garbage collector is/used to be 
written in a single C++ file.  Finally, I have hearded that C# runtime is way more
typed than the JVM.


### Fact Checking

#### C# is a direct competitor to java

**False**, it is true that both programming languages targets web servers but it is an
over simplification.  Both ecosystem has devlop in different areas. Java is used to build things
such as databases.  C# is almost uiquitous to build window's desktop applications and is used
for scripting in game engines.  

#### C# is that it is a copy of java.

**Mainly debunked**, [wikipedia][csharp] documents well that c# has (perhaps) started as a clone
but is now on a different course.

#### The language has been developped by the same person as typescript
**True**, [Anders Hejlsberg][Anders_Hejlsberg] has been leading development of both team at different time.

#### Stackoverflow is written with it and runs on an incredibly low number of servers.
**Perhaps outdated**, according to a [2014 blog post][sa-highscalability], stackoverflow runs on 
25 servers and is written in ".net".  

#### Rich Hickey said that C# async mechanism has been more inspiring then Go
**It's complicated**, ["Clojure core.async" by Rich Hickey (2013)][core-async] is where
clojure core.async is explained with great eloquence.


#### C# garbage collector is/used to be written in a single C++ file

**Dubious**, yes there is [a large .cpp file][clr-gc-github] in dotnet's repository but is it "everything"... I
cannot guanrantee.

### C# fun facts

#### Mascot
C# used to have an amazing mascot!  There's even a [github ticket][ticket-mascot] about it!

#### C# was supposed to be named "cool"
Verbatim from wikipedia:
> In January 1999, Anders Hejlsberg formed a team to build a new language at the time called Cool, which stood for "C-like Object Oriented Language"



## Important Readings

### Relation between C# and .NET

Even if [wikipedia's .NET][wikipedia-dot-net] article is small, it is good.  It explain that .NET is a run time and C# a language on that platform.






## Random Reading Notes 

### Build system

#### compiler
The compiler is named `dotnet` (not `csc`).  The program description is written in a `*csproj` xml file.

#### Single file executable
It does not seams common but creating a single file executable is possible via the and explained in https://learn.microsoft.com/en-us/dotnet/core/deploying/single-file/overview .


### Naming Conventions


**PascalCase** is not my personal favourite but seams extremly follow by the C# community. Coding covention
is well explained at https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/coding-style/coding-conventions


**_, t_ and s_ prefixes** are simply crazy.  IDE (and Microsoft sells IDEs) are way better at pin pinpointing private, static
and thread static method than conventions.  


### Language

[**get, set**][cs_properties]  keywords are quite actually.  It's a little bit like Python's `@property`
but with (in reality), reserve words.

**null operators** are amazing.  All languages that have `null` should have `?`and `??`operators.


### Runtime

**tail call optimization**, is not part of the language.  Loops must be loops.  By checking locally, call loops seam slow.

**linq** is just amazing.  This is a game changer.

## Conclusion

After about 1 day playing with C#, I am pleased to see that it lives to its hype.  C# is, to my personal taste, 
a nice language.  Something like friendlier java.






[java]: https://java.com
[csharp]: https://en.wikipedia.org/wiki/C_Sharp_(programming_language)
[ticket-mascot]: https://github.com/dotnet/csharplang/discussions/2708
[Anders_Hejlsberg]: https://en.wikipedia.org/wiki/Anders_Hejlsberg
[sa-highscalability]: http://highscalability.com/blog/2014/7/21/stackoverflow-update-560m-pageviews-a-month-25-servers-and-i.html
[core-async]: https://www.youtube.com/watch?v=yJxFPoxqzWE
[clr-gc-github]:  https://github.com/dotnet/runtime/blob/main/src/coreclr/gc/gc.cpp
[mascot-twitter]: https://twitter.com/AnsonHorton/status/1340560021390729218
[wikipedia-dot-net]: https://en.wikipedia.org/wiki/.NET

[cs_properties]: https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/properties