---
title: "L'enseignement est difficile"
date: 2024-03-11T00:00:00-00:00
draft: false
---

J'ai eu la chance d'enseigner[^4] la programmation lors de la session d'automne 2024 au [cégep Ahuntsic][ahuntsic].  L'expérience a été extraordinaire et je souhaite continuer.  Comme souvent dans la vie, j'ai été surpris à bien des égards et j'aimerais partager mon expérience.

### Mon curriculum vitae

D'abord, un peu de contexte à propos de moi.  Bien que ça fait à peu près 15 ans que je programme professionnellement, je n'ai jamais étudié l'informatique.  J'ai étudié les mathématiques où j'ai reçu un seul cours de programmation[^1].  En plus, je n'ai jamais été un étudiant très studieux.


Lors de mes emplois, j'ai mentoré beaucoup de gens. J'ai dirigé des équipes techniques.  J'ai organisé des lunch&learn et présenté un nombre incalculable de fois.

J'ai déjà essayé d'enseigner à des amis.  Avant la covide, j'ai enseigné clojure dans mon salon, j'ai enseigné les mathématiques à un ami sur un balcon et, il y a bien des années, j'ai enseigné l'algèbre à un de mes patrons.


### Ma tâche d'enseignement

Par un concours de circonstances, je me suis rendu à enseigner aux [AEC][aec] informatiques du Collège Ahuntsic.  Les cours donnés aux AEC sont en format intensifs; il s'agit de 2 ou 3 cours de 3 ou 4 heures par semaine, durant 7 ou 8 semaines.  Par exemple, le cours d'introduction à JavaScript était étalé sur 8 semaines à raison de 3 cours de 4 heures par semaine.


Les étudiants sont pour la vaste majorité des retours aux études.  Il y a beaucoup de néo-québecois ne pouvant se faire reconnaitre leurs diplômes provenant d'autres pays.  Subjectivement, pratiquement l'intégralité de mes étudiants ont été extraordinaire; j'ai enseigné à des gens super geek, à des docteurs[^2], à des vieux et des jeunes, à des gens qui avaient déjà beaucoup programmé ainsi qu'à des novices complets.


Durant ma session, j'ai enseigné un total de 5 cours[^3].  D'habitude, je donnais deux cours en même temps, des fois trois.


### Ce qui m'a surpris

Avec toutes les années que j'ai passé sur les bancs d'école, je pensais pas mal savoir à ce qui m'attendait.  Sans ordre particulier, voici une liste de points qui m'a surpris.



#### Préparation

Enseigner n'est pas présenter.  Présenter fait partie de la tâche d'un enseignant, mais il y a beaucoup plus.  Un enseignant doit baliser un chemin pédagogique pour permettre aux étudiants d'apprendre.  Donc, enseigner la programmation c'est de l'enseignement  théorique d'un langage de programmation ainsi qu'organiser des activités pratique.


Anecdotiquement, j'ai rencontré beaucoup de gens disant vouloir commencer à programmer, mais disant ne pas savoir où commencer.  La majorité de ces gens se cherchaient des projets personnels servant de motivation pour apprendre à programmer.  Un enseignant d'AEC devrait être capable de trouver ces projets personnels pour les étudiants. 


En rétrospective, je pense que j'ai énormément négligé le côté pratique de l'enseignement.  Mais il y a quelques raisons pour cela.


Premièrement, préparer des exercices pratiques me demande beaucoup plus de temps que de préparer des cours théoriques.  Je suis une machine à présentations, mais force est d'admettre que je suis plutôt mauvais à trouver des projets personnels pour les autres.  Même écrire des exercices de bases me prend énormément de temps.

De plus, demander aux étudiants de faire des travaux pratiques exige que l'enseignant soit capable d'évaluer le temps que devrait prendre un projet.  Il suffit de jeter un coup d'oeil à https://neverworkintheory.org/ pour savoir à quel point il est difficile d'*estimer* le temps que prendra un projet.  La solution que j'ai trouvée pour donner des projets est donc de briser un projet en exercices comme on le fait en mathématiques[^5].  Mais, il me semble que de briser un projet en exercices enlève l'élément "projet" tant recherché.



Deuxièmement, il m'a été plus facile d'organiser un cours en suivant un itinéraire dirigé par la théorie que la pratique.  Enseigner dans un établissement d'enseignement implique  de suivre des plans de cours et des plans cadres.  Ainsi, pour être capable de diriger le cours vers un objectif prédéfini précis, il m'a semblé plus facile de suivre la route théorique que pratique.


Je veux tout de même soulever la contradiction flagrante de la manière que j'enseigne avec la manière que j'apprends.  Lorsque j'apprends un nouveau langage de programmation, oui je lis rapidement un livre ou un blogue, mais je saute immédiatement à la pratique.  Mon github/gitlab est plein de projets en rust/zig/ruby/unison/... que j'ai fait uniquement dans le but d'apprendre un langage de programmation ou une nouvelle technologie.  Il est donc évident que j'enseigne d'une manière qui ne fonctionne pas avec les étudiants comme moi.

#### Rétroaction


L’enseignement est un travail très différent de la programmation.  Lorsque je programme, j'ai une rétroaction très rapide sur ce que je fais.  Lorsque je programme, je compile et je teste continuellement mon code pour m'assurer que je vais toujours dans la bonne direction.

Même lorsque je travaille sur un projet qui nécessite beaucoup d'architecture, je vais tout donner pour m'assurer d'avoir des "check points" me permettant de m'assurer que le projet va dans la bonne direction.


Je n'ai pas réussi à me créer des évènements de rétroactions comme j'aurais voulu.  Pour m'autoévaluer, je demande souvent à mes étudiants si tout va, j'essaye de voir dans les travaux s’il y a des points précis manqués, et je passe un sondage de satisfaction à la fin de chaque session.   Malgré tout, il m'a été très difficile d'extraire de l'information claire de tout cela.  La seule chose qui m'a été évidente d'observer c'est que je n'utilise pas assez de "stratégies pédagogiques variées"; bon, j'essaye de m'améliorer là-dessus.


De plus, user des résultats des étudiants pour s'évaluer est plus difficile que prévu.  J'ai souvent entendu de la part de mes professeurs de statistiques que la distribution des notes d'un groupe est multimodale[^7].  J'ai vu cette multimodalité dans mes cours.  Conséquemment, je ne sais pas quels étudiants je dois utiliser pour me créer un barème.  Je peux enseigner pour les plus forts, je peux enseigner pour les plus faibles ou je peux enseigner pour un public imaginaire qui représente une mesure centrale des étudiants; c'est une situation sans issue.


Ici, je manque probablement de théorie.  Probablement que j'aurais dû lire sur la pédagogie.  Il y a une science derrière l'apprentissage (la pédagogie) et il faut la respecter.  Lire de vrais livres de pédagogies est vraiment ma prochaine étape.


#### Évaluation

Évaluer mes étudiants est un cauchemar pour moi.  Qui suis-je pour donner 78% à un et 79% à un autre.

Avant d'enseigner, j'ai interviewé beaucoup trop de personnes.  Même si j'ai essayé d'être un bon intervieweur, j'ai réalisé que je suis rarement capable de distinguer un bon candidat d'un mauvais.  J'ai déjà embauché des gens en pensant qu'ils allaient être excellents alors qu'ils se sont avérés décevants; et je suis certain d'avoir rejeté énormément de bons candidats.

De ce qu'on m'a dit, pour qu'une évaluation soit bonne, il faut qu’elle évalue quantitativement des éléments vus et pratiqués en classe.  J'imagine donc qu'un examen à choix multiple serait un bon examen; mais tout le monde déteste les examens à choix multiples.

Personnellement, j'essaye de donner des travaux plus au moins guidés.  Le plus le travail est guidé, donc transformé en une série d'exercices, le plus il est facile de le noter.  Mais, comme j'ai écrit dans la section préparation, je pense que de transformer un projet en une série d'exercices diminue fortement sa valeur pédagogique.  J'essaye donc de donner des travaux qui donnent une plus grande liberté aux étudiants.  Malheureusement, cela implique que ma correction est plus ou moins arbitraire; j'essaye plus de catégoriser les examens dans des groupes que de noter proprement dit.

En rétrospective, j'en ai fait des erreurs en évaluant mes étudiants.  J'ai remis des énoncés bourrés d'erreurs, j'ai donné "l'amour" comme unique grille d'évaluation pour le travail le plus  important de la session, j'ai même utilisé la technique de l'escalier pour noter une évaluation où un soulèvement d'étudiants m'a forcé de changer mon évaluation sur le champ.  Mais je pense que je m'en suis toujours sortie. Je pense que mes étudiants ont toujours eu des notes représentatives de leurs compréhensions.  Surtout, j'ai toujours donné le plus de contexte possible aux résultats.  Il faut toujours que les étudiants soient capables d'apprendre de leurs erreurs.




#### ChatGPT


ChatGPT est l'antichrist de l'enseignement de la programmation.

Il faut comprendre que les étudiants qui suivent un cours en programmation dans un cadre d'AEC veulent un diplôme.  Et pour obtenir leurs diplômes, ils doivent passer tous leurs cours. Donc, pour certains étudiants, l'objectif ultime est de passer leur cours.

Le problème est que ChatGPT est vraiment bon à résoudre des questions de programmation. Et malheureusement, la grande partie de la note finale d'un cours de programmation provient de travaux faits à l'extérieur du cours.  Travaux que ChatGPT est capable de faire à merveille.

Ce que je pense avoir observé est troublant.  La plupart de mes groupes étaient trimodaux: "bons étudiants", "moyens étudiants" et "mauvais étudiants".  Le groupe "moyens étudiants" étant le plus petit sous-groupe.  Après quelques cours, il est relativement aisé de reconnaitre les trois sous-groups.  Lors des évaluations faites sans ChatGPT, les notes  suivent l'ordre logique: les notes des "bons étudiants" sont plus hautes que les notes des "moyens étudiants" qui sont plus hautes que les notes des "mauvais étudiants".

Lorsque les étudiants utilisent ChatGPT, les notes changent subitement.  Les meilleurs étudiants restent les meilleurs étudiants, et cela même s'ils n'utilisent pas ChatGPT.  Par contre, subitement, les "mauvais étudiants"  ont de meilleurs résultats que les "moyens étudiants".  Donc, logiquement, des  "moyens étudiants" commencent à utiliser ChatGPT.

Du point de vue des étudiants, utiliser ChatGPT est une pente glissante.  Utiliser ChatGPT permet d'avoir de meilleures notes, mais éloigne du but ultime qui est d'apprendre à programmer.  Des "moyens étudiants" peuvent donc accumuler des retards qui n'auraient jamais eu s'ils n'avaient jamais utilisé ChatGPT.

Du côté des enseignants, ils peuvent faire la course contre ChatGPT en essayant de faire des travaux que ChatGPT ne peut pas résoudre.  Je pense que cette course est stupide.  Premièrement, ChatGPT va toujours en s'améliorant.  Deuxièmement, les vrais perdants dans cette course sont les étudiants qui reçoivent des questions moins compréhensibles et qui doivent mettre davantage d'effort à comprendre les questions qu'à y répondre.



De mon côté, j'ai décidé de faire comme si ChatGPT n'existe pas.  Je ne suis pas bête, je sais que plusieurs de mes étudiants abusent de ChatGPT et apprennent plus l'art de la conversation robotique que l'art de la programmation.  Mais j'ai décidé que ce n'est pas mon problème.




[ahuntsic]: https://www.collegeahuntsic.qc.ca/
[aec]: https://www.sram.qc.ca/fr/etudier-au-collegial/programmes-etudes-collegial/formation-continue

[^1]: https://etudier.uqam.ca/cours?sigle=INF1120
[^2]: Personnes possèdant un doctorat.  Que les ordres professionnels aillent se faire voire ailleur.
[^3]: https://all-dressed-programming.com/courses/
[^4]: Dans mon vocabulaire, le terme "professeur" est réservé aux professeur universitaire.  Il
      s'agit d'un des poste les plus prestigieux de notre société et il est bon d'avoir un mot
      réservé pour cet élite.
[^5]: https://didier-ahuntsic.gitlab.io/cours-420-306-ah/_exams/devoir_001.pdf
[^7]: https://fr.wikipedia.org/wiki/Distribution_multimodale