---
title: "Finally a Solid LaTeX Alternative: Typst"
date: 2023-04-25T16:30:23-04:00
draft: false
---

## Introduction

In recent years, we have seen several game-changing software such
as [duckdb][duckdb], [web assembly][wasm] and [unison][unison] 
just to name a few.  To this list, we can now add [typst][typst].

[Typst][typst] is a modern, markdown-inspired,
typesetting system that aims to be as powerful as LaTeX.  Typst takes
as input source code to output a pdf.

This blog does not pretend to be a tutorial, but instead tries to
shed some light on an outstanding technology.

### Official Links
Here is a list of official links regarding typst.

* https://typst.app/
* https://github.com/typst/typst
* https://typst.app/docs/
* https://twitter.com/typstapp/
* https://www.linkedin.com/company/typst/
* https://www.user.tu-berlin.de/laurmaedje/programmable-markup-language-for-typesetting.pdf

### Example

Before going any further, let's present a basic typst example.


{{<typst>}}
= Easy Bread Recipe

#let cups_to_ml(nbr_cup) = int(236.588 * nbr_cup)

== Ingredients
- $4 1/2$ to 5 cups (540g to 600g) of Bread Flour
- 1 tablespoon (11g) granulated sugar
- 2 1/4 teaspoons instant yeast
- 2 1/2 teaspoons (15g) salt
- 1 2/3 cups (#cups_to_ml(1.666)ml) water, lukewarm (90°F to 110°F)

== Preparation

+ Stir together all of the ingredients in a large bowl
+ Turn the dough out onto a lightly floured surface,  
  using some of the additional 1/2 cup of flour 
  called for.
+ Place the dough in a bowl that’s been lightly
  greased with vegetable oil
+ Bake the bread for 20 to 25 minutes

#link("https://bit.ly/3LLjlTD")[Original Recipe]

{{</typst>}}


This small example displays how typst handles multiple fundamental operations:
* Creation of titles (`=`) and subtitles (`==`) 
* Creation of numbered (`+`) and bulleted lists (`-`)
* Usage of mathematical expressions (`$4 1/2$`)
* Creation of user define functions `#let cups_to_ml(nbr_cup) = int(236.588 * nbr_cup)`
* Usage of functions `#cups_to_ml(1.666)`


## The Behemoth Task Of Taking Over LaTeX

It would be naive not to talk about the elephant in the room: LaTeX.
LaTeX is one of the most incredible software ever written.  It has
been written by [Leslie Lamport][lamport] on top of [Donald Knuth][knuth]'s
Tex software.  Many people[^1][^2][^3] will say that to be a LaTeX user is to be 
in a love-hate relationship.


On one side, LaTeX is very impressive.  It allows users to create amazing
documents, it has a vast user base (even a [dedicated stack exchange][tex-stackexchange] 
about it), a huge [amount of libraries][ctan] and some good tooling built 
around it (famously the [overleaf][overleaf] online editor).  Two additional LaTeX's
selling points are the [mathematical typography][latex-math] and [bibliography][bibtex] 
support.

LaTeX bad side is that no software feel more like "we should have something
better" than it.  The syntax is very different than others languages,
error messages are cryptic, LaTeX is not trivial to use for [small documents][latex-small] 
and complexity can reach [astonomical proportions][latex-insane-large] when dealing
with large documents.

### Big Kudos to Typst Team to Tackle The Task

It is amazing that some people are starting a journey to create a real LaTeX competitor.  Creating
such a software will not be a small endeavor and will require a rare skillset and a lot of work.  


Thanks to everyone on the typst team for their efforts.


## Why is Typst Awesome

Now, let explain why typst is so amazing.

### Modern Syntax

Typst syntax seems natural to a modern programmer.  Content is written
in a markdown-inspired language.  The scripting abilities are written in a
dynamically typed language that is not far from javascript.


Below is an example of a content.  Even if it is clearly not markdown,
markdown users will not be surprised by the output.

{{< typst >}}
// example of content

= Example
This is a math equation $Q = rho A v + C$.
{{</ typst >}}

Below is a demonstration of typst scripting abilities.  The
[cons function][cons] is a "classical" linked list builder
that highlights the ease of creating advanced scripting by
using higher-order functions.

{{< typst >}}
// This is a very "lispy" implementation of a linked list
#let cons(x, lst) = {
  (head-or-rest) => if head-or-rest {x} else {lst}
}

// the list (3, 2, 1)
#let lst = cons(3, cons(2,  cons(1, none)))

The second value of the list is: #lst(false)(true)
{{</ typst >}}

#### Real Standard Library

Typst standard library is closer to nodejs or python than LaTeX.  Default
types contain [dictionnary][typst-dict] and [array][typst-array] with all
the "normal" util functions (length, add, remove,...) together with functional 
programming's bells and whistles (map, filter, ...).

Advanced functions I/O functions to load [json and csv][io] are available by
default.  Consequently, there's no more need to template LaTeX.  For some
use-cases such as generating a pdf on the backend of a webserver, this is a
big deal.


#### Switching Between Scripting And Content Context

Typst has found a nice way to switch between scripting and content context. By
default, a typst source file is in the "content" context: all text written
will be rendered in the output document.  To use a function or create one, it is
necessary to switch to scripting context.  Switching to scription context is done
via the `#` character: for instance the `one plus one equals #(1 + 1)` will 
render `one plus one equals  2` {{<download-typst-example>}} one plus one 
equals #(1 + 1) {{</download-typst-example>}}.  Inside a script context, the square 
brackets are used to re-enter the content context.
`#([*hello*]) world` will display a bold `hello world` text with bold `hello`
{{<download-typst-example>}} #([*hello*]) world {{</download-typst-example>}}.


After experimenting a bit with the system, context switching feels natural.


#### More Syntax

The best place to look for syntax examples is the [tutorial pages][tutorial].

### Great Tooling

To take over LaTeX, typst will need great tooling.  The current state of the tooling
surrounding typst is quite impressive considering the young age of the project.

The typst team is building https://typst.app/ which is a collaborative
web IDE for typst. As the author of this blog post is not very UI oriented,
the only sure thing is that it works and that it looks fantastic.  Clearly 
[overleaf][overleaf] users will like this feature a lot.

For the less UI oriented users, typst offers a [language server][lsp] 
that eases the integration with IDEs.  In the writing of this blog post, 
the [support in vscode][vscode] has been tested and is quite nice.


Typst itself is around 20MB executable written in Rust.  The source code 
is about 40 000 lines long and is open source under the Apache license.  Typst is
relatively fast to compile: a nonscientific benchmark of a part of a résumé took
around 65ms to generate a pdf while a similar document took pdflatex around 30ms 
to complete.


### Documentation and Community

[Typst documentation][typst-doc] is modern and helpfull.  There's a search
capability and plenty of examples.

Typst's team updates the community with its blog [blog][typst-blog]
and on its [discord][discord] server.  The discord server hosts a nice
Q&A section that can be helpful.

By personal experience, documentation has been enough to go by.  I have
been rewriting my résumé and at no point did I get really stucked. I
had one question that I have posted on [discord][discord] and minutes later the
problem was solved. 

## Ideas For The Future

Clearly the team behind typst are not lacking ideas for improving typst.  Nonetheless,
let's send some ideas in the universe for what typst could do next.

### Be a "Better" Programming Language

Typst scripting abilities are already amazing, Turing complete and easy to use. But,
it would be nice if Typst had a bit more features of mature progamming languages.


**Private functions:**  Currently, all functions are public and imported when 
a module is imported.  It would be nice if the author could control the
visibility of functions and variables.

**Documentation:** It would be nice if functions could have documentation similar
to javadoc attached to them.

**Package Manager:** Creating a package manager is not a small task.  But as
typst's ecosystem grows, the need will become undeniable.  A good solution 
for package management is to allow url in import locations.  [Go][go-import] and 
[javascript][js-module] uses this technique.  Many people do not like the idea of having
hard-coded urls in the source code but everyone should acknowledge the simplicity
of this method.  In addition, [deno][deno-vendoring] has demonstrated that combined
with vendoring, url imports do not impair build reproducibility.

**Testing:** Adding unit testing capabilities in typst might be quite hard.  There's
already an [assert][assert] function but a test runner is still missing.

### Create Space For Future Improvments

Typst has a lot of functions in global namespace.  For example, it as [json
data loader][typst-json], a function to [draw a circle][typst-circle], etc.
Having many functions in the global namespace is a double edged sword.  One
one part in makes many things easy; many usecases do not need any imports.  One
the otherside, it limits implementing new features and will make backward
compatibility hard to acheive for future typst's releases.

An opiniated way to deal with this issue is to leverage the fact that typst
has not yet reached the `1.0.0` point.  Consequently, users must expect backward
incompatible updates until typst reaches the `1.0.0` milestone. 

### Typst Everywhere

Currently, it seems that typst is available in two place: as command line program
and as a web application via https://typst.app/.  To make  typst a success, the
community must integrate typst everywhere! As typst is written in rust, it will
be interresting to see if [web assembly][wasm] will help run typst in more places.


## Conclusion

In my mind, the case that typst is an amazing piece of software has passed the
accepted state.  Now, the question is "will typst succeed".  Knowing that many
out of this world softwares have failed the answer is not easy.

For now, typst will need a lively community that starts using it and help
develop it.  After looking deeply into typst for this post, I do not see
any reasons to continue to use LaTeX.  Hence, I can only encourage everyone
to start to use typst right now.




[duckdb]: https://duckdb.org/
[wasm]: https://webassembly.org/
[unison]: https://www.unison-lang.org/
[typst]: https://typst.app/
[lamport]:https://en.wikipedia.org/wiki/Leslie_Lamport
[knuth]: https://en.wikipedia.org/wiki/Donald_Knuth
[tex-stackexchange]: https://tex.stackexchange.com/
[ctan]: https://www.ctan.org/
[overleaf]: https://overleaf.com
[latex-math]: https://en.wikibooks.org/wiki/LaTeX/Mathematics
[bibtex]: https://en.wikipedia.org/wiki/BibTeX
[latex-small]: https://www.overleaf.com/latex/templates/basic-template/wswqstsqbzbz
[latex-insane-large]: https://github.com/HoTT/book/
[tutorial]: https://typst.app/docs/tutorial/writing-in-typst/
[cons]: https://en.wikipedia.org/wiki/Cons
[lsp]: https://microsoft.github.io/language-server-protocol/
[vscode]: https://marketplace.visualstudio.com/items?itemName=nvarner.typst-lsp
[typst-doc]: https://typst.app/docs/
[discord]: https://discord.gg/2uDybryKPe
[assert]: https://typst.app/docs/reference/foundations/assert/
[js-module]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules
[go-import]: https://go.dev/doc/modules/managing-dependencies
[deno-vendoring]: https://deno.com/manual@v1.32.5/tools/vendor
[typst-blog]: https://typst.app/blog/
[typst-json]: https://typst.app/docs/reference/data-loading/json/
[typst-circle]: https://typst.app/docs/reference/visualize/circle/
[typst-array]: https://typst.app/docs/reference/types/array/
[typst-dict]: https://typst.app/docs/reference/types/dictionary/
[io]: https://typst.app/docs/reference/data-loading/

[^1]: https://commutative.xyz/~miguelmurca/blog/x/illihl.html
[^2]: https://www.jonasoesch.ch/articles/LaTex-a-love-hate-relationship
[^3]: https://news.ycombinator.com/item?id=31490136