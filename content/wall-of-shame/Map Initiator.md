---
title: "Map Creator"
date: 2023-02-22T14:06:36-05:00
draft: true
---


```java
private static enum DeviceCapabilityMap {

    DECLARATIONS("declarations"),
    CAN_AUTHENTICATE_CUSTOMER("can_authenticate_customer"),
    CAN_REDIRECT("can_redirect");
    private String key;

    DeviceCapabilityMap(final String key) {
        this.key = key;
    }

    private void setKey(final Map<String, Object> map, final Object value) {
        map.put(this.key, value);
    }

    private void setKey(final Map<String, String> map, final String value) {
        map.put(this.key, value);
    }

    public static Map<String, Object> buildConfig() {
        final Map<String, String> declarations = new HashMap<>();
        CAN_AUTHENTICATE_CUSTOMER.setKey(declarations, Boolean.FALSE.toString());
        CAN_REDIRECT.setKey(declarations, Boolean.TRUE.toString());
        final Map<String, Object> deviceCapabilityMap = new HashMap<>();
        DECLARATIONS.setKey(deviceCapabilityMap, declarations);
        return deviceCapabilityMap;
    }
}
```