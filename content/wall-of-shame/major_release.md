---
title: "Major Release Increment"
date: 2023-03-04T09:31:41-08:00
draft: true
---

This is the craziest definition of major release I have seen for a major library.  It
basically translates to "You can use my library, but if you use it in a way I deem unfit,
I'll throw you under the bus."

> The major release number (x) is incremented if a feature release includes a significant backward incompatible change that affects a significant fraction of users. 