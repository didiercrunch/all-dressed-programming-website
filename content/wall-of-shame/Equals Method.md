---
title: "Java Equal Method"
date: 2023-02-22T13:43:39-05:00
draft: true
---

This is an insanely creative, and totally broken implementation of 
the `equals` in java.  If you ask, the implementor of this method 
did not the write a `hashCode` method to go with the `equals` method.


```java
class Config{
    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        final Config config = (Config) obj;
        return ObjectMapper.convertToJsonNode(config)
            .equals(ObjectMapper.convertToJsonNode(obj));
    }
}
```