---
title: "L'Insoutenable Longeur Du Réseau Routier"
date: 2025-01-10T00:00:00-00:00
draft: false
tag: 'app'
---

<script>
  window.addEventListener(
    "message",
    (event) => {
      document.getElementById("yonge-street").style.height = `${event.data.height}px`
      const url = new URL(document.location.href)
      url.hash = event.data.url;
      const newURL = url.toString();
      document.location.replace(newURL);
    },
    false,
  );

  window.addEventListener("load", () => {
    const iframe = document.getElementById("yonge-street");
    const url = iframe.getAttribute('data-origin') + document.location.hash.substr(1);
    iframe.setAttribute('src', url);
  } );

</script>




<iframe data-origin="https://yonge-street.home.all-dressed-programming.com"
    style="width:100%; height:100%"
    id="yonge-street"
    frameborder="0"
    scrolling="no"

></iframe>
