---
title: "Java Mutable String"
date: 2025-01-30T0:01:00-00:00
draft: false
---


All Strings in java are immutable.  Sometime, it would be nice
to have a mutable String that would allow to manipulate Strings
without copying the underlying byte array all the time.  The goal
of this library would be to be very efficient to work with mutable
strings in java.


|                      |                      |
|:---------------------|:---------------------|
| Difficulty           | High                 |
| Programming language | Java                 |
| Project type         | Create a new library |

## How To Do it.

The goal would be to implement [CharSequence][CharSequence] around a mutable bytearray
and a [CharSet][CharSet].  The first thing to do would be to look at literature and other language
to know what to do.  The API will be tricky to implement as it must natural to work with in java while
implementing something clearly not java-esque.





[CharSequence]: https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/lang/CharSequence.html
[CharSet]: https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/nio/charset/Charset.html
