---
title: "Add Nix to pre-commit"
date: 2025-01-30T0:01:00-00:00
draft: false
---

Add Nix as language to [pre-commit][pre-commit].  In the end, any-body should be able to create
a new hook as a nix-flakes entry point.  It would be cool if regular nix package
could be used too.

|                      |                                          |
|:---------------------|:-----------------------------------------|
| Difficulty           | Low                                      |
| Project URL          | https://github.com/pre-commit/pre-commit |
| Programming language | python                                   |
| Project type         | Open source contribution                 |

## How To Do it.

Basically, we need to add two different languages to the [all_languages.py][all_languages],
one for `nix-pkgs`, one for `nix-flakes`.  The language should be super simple, just run
the specific target with the appropriate commands.  The simplest language is the [system
language][system_language] but a good example of a more complex language is the [python
language][python_language].





[nix]: https://nixos.org/
[pre-commit]: https://pre-commit.com/
[all_languages]: https://github.com/pre-commit/pre-commit/blob/c3402e66489f16f4d815640707d504bbfb444a3f/pre_commit/all_languages.py
[system_language]: https://github.com/pre-commit/pre-commit/blob/c3402e66489f16f4d815640707d504bbfb444a3f/pre_commit/languages/system.py
[python_language]: https://github.com/pre-commit/pre-commit/blob/c3402e66489f16f4d815640707d504bbfb444a3f/pre_commit/languages/python.py
