---
title: "Decent HTML Template Language"
date: 2025-02-02T0:01:00-00:00
draft: false
---


The majority of HTML template languages sucks.  Some exceptions are
rust's [html crate][html_crate], C#'s [razor][razor] and an handful
of others[^1].  All the others are glorified string formatters.

But what makes a good HTML templatating language?  First, a good
HTML templating language should **only produces valid html**.  Similarly,
it should **only accepted valid data input**.  As any good software,
a good templating language **should be composable**; it should be possible
to breakdown a large template into multiple small templates.  As this
problem is not specific to a single language, it would be nice if the
solution would be able to scale in multiple different programming
languages.



|                      |                                          |
|:---------------------|:-----------------------------------------|
| Difficulty           | Very high                                |
| Programming language | Web Assembly                             |
| Project type         | Create a new library                     |

## First Technical Ideas

|Name                  |  Definition|
|:--------------------:|:----------------------------:|
| Template             | The main document of a templating language |
| Input Data Format    | The data format a template requires to be able to be viewed as HTML |
| Compiling            | Process that takes a template as input and returns a function that can generate an HTML from data input (Renderer)   |
| Rendering            | Process of taking a compiled template and input data to generate valid HTML |
| Host Programming Language | The programming language used by the programmer |

### What is an html templating language?

The user experience for a template language is something as below:

```typescript
const template = templates.load('my-template.tmpl')
return template.render({user, book})
```

which can be summarize as a function from a `TemplateInput` to `HTML`.  Consequently,
the programmer expects 2 things from a "template"; first the template must be able
to provide the input data type, second the template must be able to render html.


### What should be the output of compiling templates

When a user renders templates, the user must provides 2 things; first the template
content, second template names. The compiler than produces a set input data format and
renderers.  The input data formats should be valid types in the host programming
language, the renderers should be functions that takes an input data format and
return a valid html.



### Crazy path

#### Step One - Defining the template
Each template has 2 sections, one defining the input data format as a
[Protobuf][protobuf] spec and the template itself. For now, we can
minimaze the template to the most trivial renderer; the hello world
renderer (a template that always returns `<h1>hello world</h1>`).

#### Step Two - Extracting the protobuf input data formats

The first thing the compiler should be able to do is to output
the input data formats in a standalone protobuf file.



#### Step Three - Continue...

Hold tight, and create a bunch of scripts that generates a rust program that
can compile into wasm function renderer.  That might not be trivial.  I assume
nix can help lock rust and cargo version in sync.  Good luck.





## Interresting Blogs

* https://lunacookies.github.io/lang/
* https://github.com/rust-analyzer/rowan
* https://www.reddit.com/r/rust/comments/18rwhdn/how_to_make_an_external_dsl_language_which_can_be/




[html_crate]: https://docs.rs/html/latest/html/
[razor]: https://learn.microsoft.com/en-us/aspnet/core/mvc/views/razor?view=aspnetcore-9.0
[protobuf]: https://protobuf.dev/


[^1]: Apparently kotlin too, zero sources.
