---
title: "Own Data Viewers"
date: 2025-02-02T0:08:00-00:00
draft: false
---

Web companies ([GAFA][gafa] & others) are forced[^1] to allow their user
to be able to dowload their data.  It is probably a pain in the ass
for there corporation.  It usually takes hours before getting the extracted
data.  Such delay shouts "expansive cross data center map reduce".


For the end users, there is not much value to download the data.  The
data is a usually a collection of machine readable  files such as JSON.

The goal of this project is to allow people to view their data exports.
The most important companies to support is probably Goole and Facebook.




|                      |                                          |
|:---------------------|:-----------------------------------------|
| Difficulty           | Medium                                   |
| Programming language | Javascript                               |
| Project type         | 100% client side application             |



It would be important to make this web application 100% client side.
No server whatsoever.





[gafa]: https://www.youtube.com/watch?v=3Ed_UIjreMo


[^1]: zero sources, but probably https://en.wikipedia.org/wiki/General_Data_Protection_Regulation
