---
title: "Is Hugo Worth Using For A Blog"
date: 2025-02-01T00:00:00-00:00
---

This blog is built on Hugo[^1] and uses the hugoloveit[^2] theme.  The three main
drivers for choosing this technology has been:
1. Being able to self host the blog
1. Minimize the need to customize style
1. Minimize the time spent coding and maximize the time spent writing


The first two points has been stellar.  This blog can be hosted on about
anything.  It is currently being hosted on deno deploy[^3] but is not
vendor locked.  Hugoloveit is the only style used; there's not a single
css file checked in the repo[^4].


Unfortunatly, this site is a collection of hacks on top of Hugo.  The
time spent fighting with Hugo has been insane.  The only reason why this
blog is maintainable is because it uses nix to bind everything together.
Here is a list of hacks done to Hugo.

* Added a custom transform function to be able to subprocess
  in templates[^5]
* Created 1 partial for the comment section  [^7]
* Created 5 shortcodes [^8]
* Created 2 sections [^9]
* Generate 1 config file programmatically  [^6]



Consequently, for similar use cases, Hugo will not be magical.  The author
of this blog has no significantly better solution to propose.


[^1]: https://gohugo.io/
[^2]: https://hugoloveit.com/categories/documentation/
[^3]: https://deno.com/deploy
[^4]: https://gitlab.com/didiercrunch/all-dressed-programming-website

[^5]: https://gitlab.com/didiercrunch/all-dressed-programming-website/-/blob/master/build-hacks/hugo_01.patch?ref_type=eb952ae0f86333efbaa44e7403186e64c2a7ee00
[^6]: https://gitlab.com/didiercrunch/all-dressed-programming-website/-/blob/master/flake.nix?ref_type=70049fb1a2b04a8261d87fad144506d260baba4e#L40
[^7]: https://gitlab.com/didiercrunch/all-dressed-programming-website/-/tree/master/layouts/partials?ref_type=70049fb1a2b04a8261d87fad144506d260baba4e
[^8]: https://gitlab.com/didiercrunch/all-dressed-programming-website/-/tree/master/layouts/shortcodes?ref_type=70049fb1a2b04a8261d87fad144506d260baba4e
[^9]: https://gitlab.com/didiercrunch/all-dressed-programming-website/-/tree/master/layouts/section?ref_type=70049fb1a2b04a8261d87fad144506d260baba4e
