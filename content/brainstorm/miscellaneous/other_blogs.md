---
title: "Links To Amazing Blogs"
date: 2023-02-25T09:20:41-05:00
draft: false
aliases:
  - /miscellaneous/statistics/
---

Here's a list of blogs that deserve attention in no particular order.

### [Lemire][1]
Daniel Lemire is the author of an amazing blog that discuss low level
computing.  In this blog, you will find answers to questions such as
"what is the fastest way know if a string is element of a small set
of strings".

[:(fa-brands fa-telegram):](https://t.me/dlemire)

### [Xe Iaso][2]
Aesthetically, this blog can be ranked first according to my standards.
Disregarding personal taste, this is the best source of knowledge about
nix that I have found.  Moreover, Xe has great content about rust, go
and so many more topics.

[:(fas fa-rss):](https://xeiaso.net/blog.rss)


### [Dragan Rocks][3]
This is the place to learn about data science and engineering in clojure.
Dragan's books are amazing.  The topic of GPU computation on the jvm is not
discussed enought and here is the place to talk about it more.

[:(fas fa-rss):](https://dragan.rocks/feed.xml)


### [BIG by Matt Stoller][4]
When I see a Matt Stoller's email in my inbox, I am happy.  The BIG news
letter is an elite, research journalism news letter about monopolies
in our society.  If you are interrested in the modern robber barrons that
rules our world, you must subscribe to the BIG news letter.


[:(fa fa-solid fa-envelope):][4]

[1]: https://lemire.me/blog/
[2]: https://xeiaso.net/blog
[3]: https://dragan.rocks/
[4]: https://mattstoller.substack.com/subscribe
