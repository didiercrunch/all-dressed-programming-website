---
title: "Godot"
date: 2023-06-03T11:25:32-04:00
draft: true
---

## Why Godot

### Game Development Vernacular

#### Heads Up Display (HUD
an informational display that appears as an overlay on top of the game view.  UI elements 
on a layer above the rest of the game, so that the information it displays isn't 
covered up by any game elements like the player or mobs