---
title: "Consultation / Freelance"
date: 2024-03-16T08:41:49-04:00
draft: false
---

### Consultation

Besoin de logiciels personnalisés? Si vous avez des problèmes avec des données ou des applications web, je peux probablement vous aider. Je prends en charge la solution dans son entièreté; l’architecture, le développement et le déploiement infonuagique. Tout le code écrit vous appartient.


Travailler avec moi est simple:

1. Vous me contactez par courriel didier.amyot@all-dressed-programming.com
2. On prend rendez-vous pour regarder le problème dans son ensemble
3. Je prends quelques jours pour planifier une solution et une tarification
4. On prend le temps de regarder la solution et la tarification ensemble pour s'assurer que tout est clair
5. J'implémente la solution le plus rapidement possible
6. On garde contact pour s’assurer que tout va bien



didier.amyot@all-dressed-programming.com

----

### Freelance

Need custom software? If you are looking for data-intensive applications or web applications, I can probably help you. I take care of everything, architecting the solution, implementing the code, and deploying the program on the most appropriate infrastructure in the cloud. All the developed assets are owned by you.

Working with me is simple:

1. you contact me by email didier.amyot@all-dressed-programming.com
2. We schedule a meeting to overview the problem
3. I take a couple of days to propose a solution and a pricing system
4. We take the time to look at the solution and the price to make sure everything is understood on both sides.
5. I implement the solution in a timely manner
6. We stay in touch to make sure everything is good


didier.amyot@all-dressed-programming.com