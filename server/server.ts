import { Application, Router, Context, Request } from "@oak/oak";
import staticFiles from "https://deno.land/x/static_files@1.1.6/mod.ts";
import { Pool } from "@dewars/postgres";

const pool = new Pool(Deno.env.get("POSTGRES_URL"), 3, true);
const router = new Router();

function getHeaders(request: Request): Record<string, string> {
  const ret: Record<string, string> = {};
  for (const [k, v] of request.headers.entries()) {
    ret[k] = v;
  }
  return ret;
}

async function error404(context: Context) {
  context.response.type = "text/html; charset=utf-8";
  context.response.status = 404;
  context.response.body = await Deno.readFile("./public/404.html");
}

router.post("/api/v1/spy", async (ctx: Context) => {
  const payload = await ctx.request.body.json();
  payload.request = {
    ip: ctx.request.ip,
    ips: ctx.request.ips,
    url: ctx.request.url.toString(),
    date: Date.now(),
    method: ctx.request.method,
    headers: getHeaders(ctx.request),
  };
  const client = await pool.connect();
  try {
    const query = "INSERT INTO blog_statistics(raw_data) VALUES ($1)";
    await client.queryArray(query, [payload]);
    ctx.response.body = "{}";
  } finally {
    client.release();
  }
});

const app = new Application();

app.use(router.routes());
app.use(staticFiles("public", { brotli: true, fetch: true }));
app.use(error404);

await app.listen({ port: 8000 });
