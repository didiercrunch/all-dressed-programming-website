{
  description = "All Dressed Programming Blog";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    highlight-typst-sources.url =
      "gitlab:all-dressed-programming/highlight-typst-sources";
    adp-nix-utils.url = "gitlab:all-dressed-programming/adp-nix-utils";
    presentations.url = "gitlab:didiercrunch/presentations";
    hugo-loveit-theme = {
      url = "github:dillonzq/LoveIt";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, flake-utils, hugo-loveit-theme
    , highlight-typst-sources, adp-nix-utils, presentations }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        hugo-overlay = final: prev: {
          hugo = prev.hugo.overrideAttrs (old: {
            patches = (old.patches or [ ]) ++ [ ./build-hacks/hugo_01.patch ];
          });
        };
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ hugo-overlay ];
        };
        highlight-typst = highlight-typst-sources.packages.${system}.default;
        writePythonApplication = adp-nix-utils.writePythonApplication.${system};
        make-mprocs = adp-nix-utils.make-mprocs.${system};

        typst-2-base64 = writePythonApplication {
          name = "typst-2-base64";
          contentFile = ./build-hacks/typst-2-base64.py;
          extraRuntimeInputs = [ pkgs.typst ];
        };

        presentation-slides-config = pkgs.stdenv.mkDerivation {
          name = "presentation-slides-config";
          src = presentations.packages.${system}.presentations;
          buildInputs = [ pkgs.bb ];
          buildPhase = ''
            echo '{"params": ' > index.json
            ${pkgs.babashka}/bin/bb index-generator.clj --base-url https://all-dressed-programming.gitlab.io/presentations/ >> index.json
            echo '}' >> index.json

          '';
          installPhase = ''
            mkdir $out
            cp index.json $out/config.json
          '';
        };

        blog = pkgs.stdenv.mkDerivation {
          name = "All-Dressed-Programming-Website";
          # Exclude themes and public fqolder from build sources
          src = builtins.filterSource (path: type:
            !(type == "directory"
              && (baseNameOf path == "themes" || baseNameOf path == "public")))
            ./.;

          # Link theme to themes folder and build
          buildInputs =
            [ pkgs.hugo pkgs.brotli highlight-typst typst-2-base64 ];
          buildPhase = ''
            mkdir -p themes
            ln -s ${hugo-loveit-theme} themes/LoveIt
            ${pkgs.hugo}/bin/hugo --minify --config config.toml,${presentation-slides-config}/config.json
            find public -type f -exec ${pkgs.brotli}/bin/brotli -9 -f -k {} \;
          '';
          installPhase = ''
            mkdir -p $out
            cp -r public $out/public
            cp -r server/* $out
            cp deno.json $out
            cp deno.lock $out
          '';

          meta = with pkgs.lib; {
            description = "All-Dressed-Programming-Website";
            platforms = platforms.all;
          };
        };

        blog-with-main = pkgs.writeShellApplication {
          name = "All-Dressed-Programming-Website";
          runtimeInputs = [ pkgs.deno blog ];
          text = ''
            cd ${blog}
            ${pkgs.deno}/bin/deno run -A ${blog}/server.ts
          '';
        };

        python-env = pkgs.python3.withPackages
          (ps: with ps; [ jupyter requests click pytest psycopg2 duckdb ]);

        local = pkgs.writeShellApplication {
          name = "local";
          runtimeInputs = [ blog pkgs.deno ];
          text = ''
            cd ${blog}
            ${pkgs.deno}/bin/deno run -A  ${blog}/server.ts
          '';
        };

        deploy = pkgs.writeShellApplication {
          name = "deploy";
          runtimeInputs = [ blog pkgs.deno ];
          text = ''
            config_dir=$(mktemp -d --suffix all-dressed-programming-deploy)
            echo "Will use $config_dir as config directory"
            cd ${blog}
            ${pkgs.deno}/bin/deno run \
                -A https://deno.land/x/deploy/deployctl.ts  deploy \
                --save-config \
                --config "$config_dir"/deno.json \
                --project=all-dressed-programming \
                ${blog}/server.ts
          '';
        };

        deploy-prod = pkgs.writeShellApplication {
          name = "deploy";
          runtimeInputs = [ blog pkgs.deno ];
          text = ''
            config_dir=$(mktemp -d --suffix all-dressed-programming-deploy)
            echo "Will use $config_dir as config directory"
            cd ${blog}
            ${pkgs.deno}/bin/deno run \
                -A https://deno.land/x/deploy/deployctl.ts  deploy \
                --save-config \
                --config "$config_dir"/deno.json \
                --project=all-dressed-programming \
                --prod \
                ${blog}/server.ts
          '';
        };

        serve-hugo = pkgs.writeShellApplication {
          name = "serve-hugo";
          runtimeInputs = [ pkgs.hugo ];
          text =
            "hugo serve --disableFastRender --buildDrafts --renderStaticToDisk";
        };

        watch-dev = make-mprocs {
          name = "watch-dev";
          derivations = [ serve-hugo ];
        };
      in {
        packages = {
          typst-2-base64 = typst-2-base64;
          blog = blog;
          deploy-prod = deploy-prod;
          deploy = deploy;
          local = local;
          default = blog-with-main;
          presentation-slides-config = presentation-slides-config;
        };

        devShells.default = pkgs.mkShell {
          buildInputs = with pkgs; [
            hugo
            deno
            python-env
            gitleaks
            postgresql_15
            typst-2-base64
            highlight-typst
            watch-dev
          ];
          # Link theme to themes folder
          shellHook = ''
            mkdir -p themes
            if [ ! -L themes/LoveIt ]; then
              ln -sf ${hugo-loveit-theme} themes/LoveIt
            fi
          '';
        };
      });
}
