# ;; this is a babashka script.. because.. why not?
# (require '[babashka.fs :as fs]
#          '[babashka.process :as process])
#
# (let [tmp-file-input (fs/create-temp-file)
#       tmp-file-output (fs/create-temp-file)
#       cmd (str "typst compile " tmp-file-input " " tmp-file-output)]
#      (spit (str tmp-file-input)  *input*)
#      (process/shell cmd)
#      (println (slurp (str tmp-file-output))))
#
#
from io import IOBase, StringIO, BytesIO
from tempfile import TemporaryDirectory
from pathlib import Path
import subprocess
import shutil
import sys
import base64


def compile_with_typst(input_file: Path, output_file: Path) -> None:
    cmd = ['typst', 'compile', str(input_file.absolute()), str(output_file.absolute())]
    subprocess.check_call(cmd)


def copy_file_object_to_file(src: IOBase, dest: Path) -> None:
    with open(dest, 'w') as file_:
        shutil.copyfileobj(src, file_)


def main(std_in: IOBase, work_dir: Path) -> None:
   input_file = work_dir / 'input.typ'
   output_file = work_dir / 'output.pdf'
   copy_file_object_to_file(std_in, input_file)
   compile_with_typst(input_file, output_file)
   with open(output_file, 'rb') as file_:
       s = BytesIO()
       base64.encode(file_, s)
       print(s.getvalue().replace(b'\n', b'').decode('ascii'))


if __name__ == '__main__':
    with TemporaryDirectory() as work_dir:
        main(sys.stdin, Path(work_dir))
