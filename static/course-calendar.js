import m from "https://esm.sh/mithril@2.2.2"
import {DateTime} from "https://esm.sh/luxon@3.4.3"

const strokeWidth = 4;

function* circleCxs(number, containerWith, circleRadius) {
    const effectiveRadius = circleRadius + strokeWidth;
    for (let cx = effectiveRadius; cx + effectiveRadius < containerWith; cx += 2 * effectiveRadius) {
        yield cx;
    }
}

function* circleCys(circleRadius) {
    const effectiveRadius = circleRadius + strokeWidth;
    yield effectiveRadius
    yield 3 * effectiveRadius
}

function* circlePos(number, containerWith, circleRadius) {
    let counter = 0
    for (const cy of circleCys(circleRadius)) {
        for (const cx of circleCxs(number, containerWith, circleRadius)) {
            if (counter >= number) {
                return;
            }
            yield [counter, cx, cy];
            counter++;
        }
    }
}

function getCircleStyles(d) {
    const ret = {
        fill: d.isInThePass ? '#5d5d5f' : '#a9a9b3',
        title: `Cours numéro`
    };
    if (d.devoir) {
        ret['stroke-width'] = strokeWidth;
        ret.stroke = d.isInThePass ? '#2e677c' : '#55bde2';
    }
    if (d.force_majeur){
        ret['stroke-width'] = strokeWidth;
        ret.stroke = d.isInThePass ? '#5eedb6' : '#5eed6f';
        ret.title = "force majeur";
    }
    return ret;
}

function wrapInLink(circle, datum) {
    if (!datum.link) {
        return circle;
    }
    return m('a', {href: datum.link}, circle);
}

function createCircleLabel(x, y, i) {
    const params = {
        x: x,
        y: y,
        dy: '0.3em',
        'text-anchor': 'middle',
        fill: '#292a2d'
    };
    return m("text", params, `${i + 1}`)
}

function createCircle(cx, cy, circleRadius, datum){
    const params = {cx: cx, cy: cy, r: circleRadius, ...getCircleStyles(datum)};
    return m("circle", params);
}


function* createChart(data, containerWith, circleRadius) {
    for (const [i, cx, cy] of circlePos(data.length, containerWith, circleRadius)) {
        const g = m("g",
            createCircle(cx, cy, circleRadius, data[i]),
            createCircleLabel(cx, cy, i));
        yield wrapInLink(g, data[i]);
    }
}

function parseDate(date){
    if(date instanceof Date){
        return DateTime.fromJSDate(date);
    }
    return DateTime.fromISO(date);
}

function parseRawDatum(rawDatum, currentTime) {
    const ret = {...rawDatum};
    ret.date = parseDate(rawDatum.date);
    ret.isInThePass = ret.date.diff(currentTime).values.milliseconds < 0;
    return ret;
}

function calendarRenderer(root, rawData) {
    const rootWidth = root.width.baseVal.value;
    const now = DateTime.now();
    const remValue = parseInt(getComputedStyle(window.document.documentElement).fontSize);
    const data = rawData.map((d) => parseRawDatum(d, now));
    m.render(root, [...createChart(data, rootWidth, remValue)]);
}

export default calendarRenderer;
