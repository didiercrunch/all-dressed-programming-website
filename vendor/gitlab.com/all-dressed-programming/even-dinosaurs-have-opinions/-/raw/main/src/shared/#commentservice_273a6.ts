export enum CommentPermission {
	Editor = 'editor',
	Viewer = 'viewer',
}

export interface Comment {
	comment: string;
	id: string;
	numberOfUpVote: number;
	numberOfDownVote: number;
	hasVotedUp: boolean;
	hasVotedDown: boolean;
	author: string;
	date: Date;
	secret: string;
	permission: CommentPermission;
}

export interface CommentVotes {
	up: string[];
	down: string[];
}

export interface SerializableComment {
	comment: string;
	id: string;
	votes: CommentVotes;
	author: string;
	creationTimestamp: number;
	secret: string;
}

export interface CommentService {
	getComments(secret: string): Promise<Comment[]>;
	postComment(
		comment: string,
		author: string,
		secret: string,
	): Promise<Comment>;
	voteUp(id: string, secret: string): Promise<Comment | null>;
	cancelVoteUp(id: string, secret: string): Promise<Comment | null>;
	voteDown(id: string, secret: string): Promise<Comment | null>;
	cancelVoteDown(id: string, secret: string): Promise<Comment | null>;
	trash(commentId: string, secret: string): Promise<boolean>;
}

export interface UserCommentAction {
	secret: string;
	action: CommentAction;
	value: CommentValue;
}

export enum CommentAction {
	Vote = 'vote',
	CancelVote = 'cancel-vote',
}

export enum CommentValue {
	Up = 'up',
	Down = 'down',
}

export class HMACService {
	concatArrayBuffer(...buffers: ArrayBuffer[]): ArrayBuffer {
		if (buffers.length == 0) {
			return new ArrayBuffer(0);
		}
		if (buffers.length == 1) {
			return buffers[0];
		}
		const buffer1 = buffers[0];
		const buffer2 = buffers[1];
		const tmp = new Uint8Array(buffer1.byteLength + buffer2.byteLength);
		tmp.set(new Uint8Array(buffer1), 0);
		tmp.set(new Uint8Array(buffer2), buffer1.byteLength);
		return this.concatArrayBuffer(tmp.buffer, ...buffers.splice(2));
	}

	hexRepresentation(buffer: ArrayBuffer): string {
		return [...new Uint8Array(buffer)]
			.map((x) => x.toString(16).padStart(2, '0'))
			.join('');
	}

	async hmac(id: string, secret: string): Promise<string> {
		const algorithm = 'sha-256';
		const encoder = new TextEncoder();
		const hashedSecret = await crypto.subtle.digest(
			algorithm,
			encoder.encode(secret),
		);
		const payload = this.concatArrayBuffer(
			hashedSecret,
			await crypto.subtle.digest(
				algorithm,
				this.concatArrayBuffer(hashedSecret, encoder.encode(id)),
			),
		);
		const ret = await crypto.subtle.digest(algorithm, payload);
		return this.hexRepresentation(ret);
	}
}

export class CommentAuthenticationService {
	private hmacService = new HMACService();
	private cache = new Map<string, string>();

	private async hmac(
		commentId: string,
		creatorSecret: string,
	): Promise<string> {
		const key = JSON.stringify([commentId, creatorSecret]);
		const cachedValue = this.cache.get(key);
		if (cachedValue) {
			return cachedValue;
		}
		const ret = await this.hmacService.hmac(commentId, creatorSecret);
		this.cache.set(key, ret);
		return ret;
	}

	async computeSecret(
		commentId: string,
		creatorSecret: string,
	): Promise<string> {
		return await this.hmac(commentId, creatorSecret);
	}

	async computePublicSecret(voterSecret: string): Promise<string> {
		const salt = 'quand-j-ai-un-petit-creux-j-aime-la-ratatouille';
		return await this.hmac(voterSecret, salt);
	}
}

export class CommentAdminService {
	private commentAuthenticationService = new CommentAuthenticationService();

	async CreateComment(
		commentText: string,
		author: string,
		secret: string,
	): Promise<SerializableComment> {
		const commentId = self.crypto.randomUUID();
		const commentSecret = await this.commentAuthenticationService.computeSecret(
			commentId,
			secret,
		);
		return {
			comment: commentText,
			author: author,
			id: commentId,
			secret: commentSecret,
			votes: { up: [], down: [] },
			creationTimestamp: Date.now(),
		};
	}
}

class CommentServiceUtil {

	constructor(private commentAuthenticationService: CommentAuthenticationService){}

	async copyComment(
		ser: SerializableComment,
		secret: string,
	): Promise<Comment> {
		const foo = await this.commentAuthenticationService.computeSecret(
			ser.id,
			secret,
		);
		const ps = await this.commentAuthenticationService.computePublicSecret(
			secret,
		);
		return {
			id: ser.id,
			author: ser.author,
			comment: ser.comment,
			date: new Date(ser.creationTimestamp),
			secret: ser.secret,
			permission: foo == ser.secret ? CommentPermission.Editor : CommentPermission.Viewer,
			numberOfDownVote: ser.votes.down.length,
			numberOfUpVote: ser.votes.up.length,
			hasVotedUp: ser.votes.up.indexOf(ps) != -1,
			hasVotedDown: ser.votes.down.indexOf(ps) != -1,
		};
	}
}

export class LocalStorageCommentService implements CommentService {
	private comments: SerializableComment[] = [];
	private commentAdminService = new CommentAdminService();
	private commentAuthenticationService = new CommentAuthenticationService();
	private readonly commentServiceUtil: CommentServiceUtil;
	private readonly commentsId: string;

	constructor(commentsId: string) {
		this.commentsId = commentsId;
		this.commentServiceUtil = new CommentServiceUtil(this.commentAuthenticationService)
	}

	private get localStorageLocation() {
		return `comments/${this.commentsId}`;
	}

	saveComments() {
		const serializedComments = JSON.stringify(this.comments);
		window.localStorage.setItem(this.localStorageLocation, serializedComments);
	}

	loadComments = async (secret: string): Promise<void> => {
		const serializedComments = window.localStorage.getItem(this.localStorageLocation) ||
			JSON.stringify([]);
		this.comments = JSON.parse(serializedComments);
	};

	getComments = async (secret: string): Promise<Comment[]> => {
		const publicSecret = await this.commentAuthenticationService
			.computePublicSecret(secret);
		if (this.comments.length < 1) {
			await this.loadComments(secret);
		}
		const ret = this.comments.map((ser) => this.commentServiceUtil.copyComment(ser, publicSecret));
		return Promise.all(ret);
	};

	async getCommentWithEditorPermission(
		id: string,
		secret: string,
	): Promise<Comment | null> {
		for (const comment of await this.getComments(secret)) {
			if (comment.id == id && comment.permission == CommentPermission.Editor) {
				return comment;
			}
		}
		return null;
	}

	async doToComment(
		id: string,
		secret: string,
		f: (c: Comment) => void,
	): Promise<Comment | null> {
		const comment = await this.getCommentWithEditorPermission(id, secret);
		if (!comment) {
			return Promise.resolve(null);
		}
		f(comment);
		this.saveComments();
		return Promise.resolve(comment);
	}

	cancelVoteUp(id: string, secret: string): Promise<Comment | null> {
		return this.doToComment(
			id,
			secret,
			(comment) => comment.numberOfUpVote -= 1,
		);
	}

	cancelVoteDown(id: string, secret: string): Promise<Comment | null> {
		return this.doToComment(
			id,
			secret,
			(comment) => comment.numberOfDownVote -= 1,
		);
	}

	voteUp(id: string, secret: string): Promise<Comment | null> {
		return this.doToComment(
			id,
			secret,
			(comment) => comment.numberOfUpVote += 1,
		);
	}

	voteDown(id: string, secret: string): Promise<Comment | null> {
		return this.doToComment(
			id,
			secret,
			(comment) => comment.numberOfDownVote += 1,
		);
	}

	async postComment(
		commentText: string,
		author: string,
		secret: string,
	): Promise<Comment> {
		const comment = await this.commentAdminService.CreateComment(
			commentText,
			author,
			secret,
		);
		this.comments.push(comment);
		this.saveComments();
		return await this.commentServiceUtil.copyComment(comment, secret);
	}

	private removeComment(commentId: string): boolean {
		const isMatch = (c: SerializableComment): boolean => c.id == commentId;
		const originalSize = this.comments.length;
		this.comments = this.comments.filter((c) => !isMatch(c));
		this.saveComments();
		return this.comments.length != originalSize;
	}

	async trash(commentId: string, secret: string): Promise<boolean> {
		const comment = await this.getCommentWithEditorPermission(
			commentId,
			secret,
		);
		if (comment == null) {
			return Promise.resolve(false);
		}
		return Promise.resolve(this.removeComment(commentId));
	}
}

export class RemoteStorageCommentService implements CommentService {
	private readonly commentAuthenticationService = new CommentAuthenticationService();
	private readonly baseURL: string;
	private readonly pageId: string;
	private readonly commentServiceUtil: CommentServiceUtil;

	constructor(pageId: string, apiBaseId: string) {
		this.pageId = pageId.replaceAll(/(\.|\/)/g, '-');
		this.baseURL = `${apiBaseId}/v1/${this.pageId}`;
		this.commentServiceUtil = new CommentServiceUtil(this.commentAuthenticationService)
	}

	async getComments(secret: string): Promise<Comment[]> {
		const resp = await fetch(this.baseURL + '/comments');
		const payload = await resp.json();
		const ret = payload.comments.map((ser: SerializableComment) => this.commentServiceUtil.copyComment(ser, secret));
		return Promise.all(ret);
	}

	async postComment(
		comment: string,
		author: string,
		secret: string,
	): Promise<Comment> {
		const data = { author: author, comment: comment, authorSecret: secret };
		const resp = await fetch(this.baseURL + '/comments', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(data),
		});
		const ser = await resp.json();
		return await this.commentServiceUtil.copyComment(ser, secret);
	}

	private async postOnComment(
		id: string,
		secret: string,
		data: UserCommentAction,
	): Promise<Comment> {
		const resp = await fetch(`${this.baseURL}/comments/${id}`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(data),
		});
		const ser = await resp.json();
		return await this.commentServiceUtil.copyComment(ser, secret);
	}

	async voteUp(id: string, secret: string): Promise<Comment> {
		const data: UserCommentAction = {
			action: CommentAction.Vote,
			value: CommentValue.Up,
			secret,
		};
		return await this.postOnComment(id, secret, data);
	}

	async cancelVoteUp(id: string, secret: string): Promise<Comment> {
		const data: UserCommentAction = {
			action: CommentAction.CancelVote,
			value: CommentValue.Up,
			secret,
		};
		return await this.postOnComment(id, secret, data);
	}

	async voteDown(id: string, secret: string): Promise<Comment> {
		const data: UserCommentAction = {
			action: CommentAction.Vote,
			value: CommentValue.Down,
			secret,
		};
		return await this.postOnComment(id, secret, data);
	}

	async cancelVoteDown(id: string, secret: string): Promise<Comment> {
		const data: UserCommentAction = {
			action: CommentAction.CancelVote,
			value: CommentValue.Down,
			secret,
		};
		return await this.postOnComment(id, secret, data);
	}

	async trash(commentId: string, secret: string): Promise<boolean> {
		const resp = await fetch(`${this.baseURL}/comments/${commentId}`, {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({ secret }),
		});
		return true;
	}
}
