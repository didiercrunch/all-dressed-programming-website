import * as postgres from '@dewars/postgres/';
import {Request, Router, Status} from '@oak/oak/';

import {
    CommentAdminService,
    CommentAuthenticationService,
    CommentVotes,
    SerializableComment,
} from '../shared/commentService.ts';

export interface CommentLocation {
    domain: string;
    pageId: string;
}

export interface CreateCommitPayload {
    author: string;
    comment: string;
    authorSecret: string;
}

interface PostgresCommentRow {
    id: string;
    domain: string;
    page_id: string;
    secret: string;
    author: string;
    raw_comment_body: string;
    vote_up: string[];
    vote_down: string[];
    created_at: Date;
}

export interface CommentStorageService {
    listComment(location: CommentLocation): Promise<SerializableComment[]>;

    createComment(
        location: CommentLocation,
        payload: CreateCommitPayload,
    ): Promise<SerializableComment>;

    getComment(
        location: CommentLocation,
        commentId: string,
    ): Promise<SerializableComment | null>;

    voteUp(
        loc: CommentLocation,
        commentId: any,
        voterSecret: string,
    ): Promise<SerializableComment | null>;

    voteDown(
        loc: CommentLocation,
        commentId: any,
        voterSecret: string,
    ): Promise<SerializableComment | null>;

    cancelVoteUp(
        loc: CommentLocation,
        commentId: string,
        voterSecret: string,
    ): Promise<SerializableComment | null>;

    cancelVoteDown(
        loc: CommentLocation,
        commentId: string,
        voterSecret: string,
    ): Promise<SerializableComment | null>;

    deleteComment(
        location: CommentLocation,
        commentId: string,
        voterSecret: string,
    ): Promise<boolean>;
}

export class PostgresCommentStorageService implements CommentStorageService {
    private commentAdminService = new CommentAdminService();
    private commentAuthenticationService = new CommentAuthenticationService();
    private pool: postgres.Pool;

    constructor(pool: postgres.Pool) {
        this.pool = pool;
    }

    private async getClient(): Promise<postgres.PoolClient> {
        return this.pool.connect();
    }

    private rowToSerializableComment(
        row: PostgresCommentRow,
    ): SerializableComment {
        return {
            id: row.id,
            comment: row.raw_comment_body,
            author: row.author,
            secret: row.secret,
            votes: { up: row.vote_up, down: row.vote_down },
            creationTimestamp: row.created_at.getTime(),
        };
    }

    async listComment(location: CommentLocation): Promise<SerializableComment[]> {
        const client = await this.getClient();
        try {
            const ret = await client.queryObject<PostgresCommentRow>(
                `SELECT *
                 FROM visible_comments_with_votes 
                 WHERE domain = $1 
                   AND page_id = $2`,
                [location.domain, location.pageId],
            );
            return ret.rows.map(this.rowToSerializableComment);
        } finally {
            client.release();
        }
    }

    async createComment(
        location: CommentLocation,
        payload: CreateCommitPayload,
    ): Promise<SerializableComment> {
        const client = await this.getClient();
        try {
            const ser = await this.commentAdminService.CreateComment(
                payload.comment,
                payload.author,
                payload.authorSecret,
            );
            const query = `INSERT INTO comments(id, domain, page_id, raw_comment_body, secret, author, created_at) 
                           VALUES ($1, $2, $3, $4, $5, $6, CURRENT_TIMESTAMP)
                           RETURNING created_at`;
            const params = [
                ser.id,
                location.domain,
                location.pageId,
                ser.comment,
                ser.secret,
                ser.author,
            ];
            const ret = await client.queryObject<{ created_at: Date }>(query, params);
            ser.creationTimestamp = ret.rows[0].created_at.getTime();
            return ser;
        } finally {
            client.release();
        }
    }

    private async getCommentUnsafe(
        client: postgres.Client,
        location: CommentLocation,
        commentId: string,
    ): Promise<SerializableComment | null> {
        const ret = await client.queryObject<PostgresCommentRow>(
            `SELECT *
             FROM visible_comments_with_votes 
             WHERE domain = $1 
                   AND page_id = $2 
                   AND id = $3`,
            [location.domain, location.pageId, commentId],
        );
        return ret.rows.length < 1 ? null : this.rowToSerializableComment(ret.rows[0]);
    }

    async getComment(
        location: CommentLocation,
        commentId: string,
    ): Promise<SerializableComment | null> {
        const client = await this.getClient();
        try {
            return this.getCommentUnsafe(client, location, commentId);
        } finally {
            client.release();
        }
    }

    private async vote(
        loc: CommentLocation,
        commentId: any,
        voterSecret: string,
        vote: keyof CommentVotes,
    ): Promise<SerializableComment | null> {
        const voter_public_secret = await this.commentAuthenticationService
            .computePublicSecret(voterSecret);
        const client = await this.getClient();
        try {
            const ret = await client.queryObject<PostgresCommentRow>(
                `INSERT INTO comment_votes(comment_id, voter_public_secret, vote_value)
                 VALUES ($1, $2, $3)`,
                [commentId, voter_public_secret, vote],
            );
            return await this.getCommentUnsafe(client, loc, commentId);
        } finally {
            client.release();
        }
    }

    async voteUp(
        loc: CommentLocation,
        commentId: any,
        voterSecret: string,
    ): Promise<SerializableComment | null> {
        return await this.vote(loc, commentId, voterSecret, 'up');
    }

    async voteDown(
        loc: CommentLocation,
        commentId: any,
        voterSecret: string,
    ): Promise<SerializableComment | null> {
        return await this.vote(loc, commentId, voterSecret, 'down');
    }

    private async cancelVoteFor(
        loc: CommentLocation,
        commentId: string,
        voterSecret: string,
        vote: keyof CommentVotes,
    ) {
        const voter_public_secret = await this.commentAuthenticationService
            .computePublicSecret(voterSecret);
        const client = await this.getClient();
        try {
            const ret = await client.queryObject<PostgresCommentRow>(
                `DELETE FROM comment_votes 
                 WHERE comment_id = $1 AND voter_public_secret = $2 AND vote_value = $3`,
                [commentId, voter_public_secret, vote],
            );
            return await this.getCommentUnsafe(client, loc, commentId);
        } finally {
            client.release();
        }
    }

    async cancelVoteUp(
        loc: CommentLocation,
        commentId: string,
        voterSecret: string,
    ): Promise<SerializableComment | null> {
        return await this.cancelVoteFor(loc, commentId, voterSecret, 'up');
    }

    async cancelVoteDown(
        loc: CommentLocation,
        commentId: string,
        voterSecret: string,
    ): Promise<SerializableComment | null> {
        return await this.cancelVoteFor(loc, commentId, voterSecret, 'down');
    }

    async deleteComment(
        location: CommentLocation,
        commentId: string,
        voterSecret: string,
    ): Promise<boolean> {
        const commentSecret = await this.commentAuthenticationService.computeSecret(
            commentId,
            voterSecret,
        );
        const client = await this.getClient();
        try {
            const ret = await client.queryObject(
                `UPDATE comments SET status = 'deleted'
                 WHERE id = $1 AND secret = $2`,
                [commentId, commentSecret],
            );
            return true;
        } finally {
            client.release();
        }
    }
}

export class LocalStorageCommentStorageService implements CommentStorageService {
    private readonly commentAdminService = new CommentAdminService();
    private readonly commentAuthenticationService = new CommentAuthenticationService();

    private getKey(location: CommentLocation): string {
        return `${location.domain}/${location.pageId}`;
    }

    private async updateComment(
        location: CommentLocation,
        comment: SerializableComment,
    ): Promise<SerializableComment> {
        const comments = await this.listComment(location);
        for (let i = 0; i < comments.length; i++) {
            if (comments[i].id === comment.id) {
                comments[i] = comment;
                break;
            }
        }
        const key = this.getKey(location);
        localStorage.setItem(key, JSON.stringify(comments));
        return comment;
    }

    listComment(location: CommentLocation): Promise<SerializableComment[]> {
        const key = this.getKey(location);
        return Promise.resolve(JSON.parse(localStorage.getItem(key) || '[]'));
    }

    async getComment(
        location: CommentLocation,
        id: string,
    ): Promise<SerializableComment | null> {
        for (const comment of await this.listComment(location)) {
            if (comment.id == id) {
                return comment;
            }
        }
        return null;
    }

    async createComment(
        location: CommentLocation,
        payload: CreateCommitPayload,
    ): Promise<SerializableComment> {
        const ser = await this.commentAdminService.CreateComment(
            payload.comment,
            payload.author,
            payload.authorSecret,
        );
        const data = await this.listComment(location);
        data.unshift(ser);
        const key = this.getKey(location);
        localStorage.setItem(key, JSON.stringify(data));
        return ser;
    }

    private async voteLogic(
        loc: CommentLocation,
        commentId: string,
        voterSecret: string,
        logic: (comment: SerializableComment, voter: string) => void,
    ): Promise<SerializableComment | null> {
        const comment = await this.getComment(loc, commentId);
        if (!comment) {
            return null;
        }
        const voter = await this.commentAuthenticationService.computePublicSecret(
            voterSecret,
        );
        logic(comment, voter);
        return await this.updateComment(loc, comment);
    }

    private async voteFor(
        loc: CommentLocation,
        commentId: string,
        voterSecret: string,
        target: keyof CommentVotes,
    ) {
        return await this.voteLogic(
            loc,
            commentId,
            voterSecret,
            (comment, voter) => {
                comment.votes[target] = Array.from(
                    new Set(comment.votes[target]).add(voter),
                );
            },
        );
    }

    private async cancelVoteFor(
        loc: CommentLocation,
        commentId: string,
        voterSecret: string,
        target: keyof CommentVotes,
    ) {
        return await this.voteLogic(
            loc,
            commentId,
            voterSecret,
            (comment, voter) => {
                const votes = new Set(comment.votes[target]);
                votes.delete(voter);
                comment.votes[target] = Array.from(votes);
            },
        );
    }

    async voteUp(
        loc: CommentLocation,
        commentId: string,
        voterSecret: string,
    ): Promise<SerializableComment | null> {
        return await this.voteFor(loc, commentId, voterSecret, 'up');
    }

    async cancelVoteUp(
        loc: CommentLocation,
        commentId: string,
        voterSecret: string,
    ): Promise<SerializableComment | null> {
        return await this.cancelVoteFor(loc, commentId, voterSecret, 'up');
    }

    async voteDown(
        loc: CommentLocation,
        commentId: string,
        voterSecret: string,
    ): Promise<SerializableComment | null> {
        return await this.voteFor(loc, commentId, voterSecret, 'down');
    }

    async cancelVoteDown(
        loc: CommentLocation,
        commentId: string,
        voterSecret: string,
    ): Promise<SerializableComment | null> {
        return await this.cancelVoteFor(loc, commentId, voterSecret, 'down');
    }

    private shouldBeDeleted(comment: SerializableComment, commentId: string, commentSecret: string): boolean {
        if (comment.id !== commentId) {
            return false;
        }
        return comment.secret === commentSecret;
    }

    async deleteComment(
        location: CommentLocation,
        commentId: string,
        voterSecret: string,
    ): Promise<boolean> {
        const comments = await this.listComment(location);
        const initialSize = comments.length;
        const commentSecret = await this.commentAuthenticationService.computeSecret(
            commentId,
            voterSecret,
        );
        const ret = comments.filter(x => !this.shouldBeDeleted(x, commentId, commentSecret))

        const key = this.getKey(location);
        localStorage.setItem(key, JSON.stringify(ret));
        return initialSize !== ret.length;
    }
}

type Domain = string;
type PageId = string;
type CommentId = string; // a uuid
type KvCommentKey = ['v1', 'comments', Domain, PageId, CommentId];
// type KvCommentKeyIndex = ['v1', 'increasing-index', 'comments', Domain, PageId];

export class DenoKVStorageCommentStorageService implements CommentStorageService {
    private readonly commentAdminService = new CommentAdminService();
    private readonly commentAuthenticationService = new CommentAuthenticationService();

    constructor(private kv: Deno.Kv) {}

    getKv(): Promise<Deno.Kv> {
        return Promise.resolve(this.kv);
    }

    getCommentKey(location: CommentLocation, commentId: string): KvCommentKey {
        return ['v1', 'comments', location.domain, location.pageId, commentId];
    }

    private insertCommentUnsafe(
        kv: Deno.Kv,
        location: CommentLocation,
        comment: SerializableComment,
    ): Promise<Deno.KvCommitResult | Deno.KvCommitError> {
        const key: KvCommentKey = this.getCommentKey(location, comment.id);
        return kv.set(key, comment);
    }

    private async tryNTimes(
        transaction: () => Promise<Deno.KvCommitResult | Deno.KvCommitError>,
        n: number,
    ): Promise<void> {
        for (let i = 0; i < n; i++) {
            const res = await transaction();
            if (res.ok) {
                return;
            }
        }
    }

    async createComment(location: CommentLocation, payload: CreateCommitPayload): Promise<SerializableComment> {
        const comment = await this.commentAdminService.CreateComment(
            payload.comment,
            payload.author,
            payload.authorSecret,
        );
        const kv = await this.getKv();
        await this.tryNTimes(
            () => this.insertCommentUnsafe(kv, location, comment),
            10,
        );
        return comment;
    }

    async deleteComment(location: CommentLocation, commentId: string, voterSecret: string): Promise<boolean> {
        const commentSecret = await this.commentAuthenticationService.computeSecret(
            commentId,
            voterSecret,
        );
        const key = this.getCommentKey(location, commentId);
        const kv = await this.getKv();
        const res = await kv.get<SerializableComment>(key);
        if (res.value === null) {
            return false;
        }
        if (res.value.secret !== commentSecret) {
            return false;
        }
        const transact = await kv.atomic().check(res).delete(key).commit();
        return transact.ok;
    }

    async getComment(location: CommentLocation, commentId: string): Promise<SerializableComment | null> {
        const kv = await this.getKv();
        const key = this.getCommentKey(location, commentId);
        const res = await kv.get<SerializableComment>(key);
        return res.value;
    }

    async listComment(location: CommentLocation): Promise<SerializableComment[]> {
        const kv = await this.getKv();
        const iter = kv.list<SerializableComment>({ prefix: ['v1', 'comments', location.domain, location.pageId] }, {
            limit: 25,
        });
        const ret: SerializableComment[] = [];
        for await (const e of iter) {
            ret.push(e.value);
        }
        return ret;
    }

    private async updateComment(
        location: CommentLocation,
        comment: SerializableComment,
    ): Promise<Deno.KvCommitResult | Deno.KvCommitError> {
        const kv = await this.getKv();

        const key = this.getCommentKey(location, comment.id);
        const commentRes = await kv.get<SerializableComment>(key);
        return await kv.atomic()
            .check(commentRes) // Ensure the sender's balance hasn't changed.
            .set(key, comment) // Update the sender's balance.
            .commit();
    }

    private async voteLogic(
        loc: CommentLocation,
        commentId: string,
        voterSecret: string,
        logic: (comment: SerializableComment, voter: string) => SerializableComment,
    ): Promise<SerializableComment | null> {
        const comment = await this.getComment(loc, commentId);
        if (!comment) {
            return null;
        }
        const voter = await this.commentAuthenticationService.computePublicSecret(
            voterSecret,
        );
        const newComment = logic(comment, voter);
        await this.updateComment(loc, newComment);
        return newComment;
    }

    private async voteFor(
        loc: CommentLocation,
        commentId: string,
        voterSecret: string,
        target: keyof CommentVotes,
    ) {
        return await this.voteLogic(
            loc,
            commentId,
            voterSecret,
            (comment, voter) => {
                comment.votes[target] = Array.from(
                    new Set(comment.votes[target]).add(voter),
                );
                return comment;
            },
        );
    }

    private async cancelVoteFor(
        loc: CommentLocation,
        commentId: string,
        voterSecret: string,
        target: keyof CommentVotes,
    ) {
        return await this.voteLogic(
            loc,
            commentId,
            voterSecret,
            (comment, voter) => {
                const votes = new Set(comment.votes[target]);
                votes.delete(voter);
                comment.votes[target] = Array.from(votes);
                return comment;
            },
        );
    }

    voteDown(loc: CommentLocation, commentId: any, voterSecret: string): Promise<SerializableComment | null> {
        return this.voteFor(loc, commentId, voterSecret, 'down');
    }

    voteUp(loc: CommentLocation, commentId: any, voterSecret: string): Promise<SerializableComment | null> {
        return this.voteFor(loc, commentId, voterSecret, 'up');
    }

    cancelVoteDown(loc: CommentLocation, commentId: string, voterSecret: string): Promise<SerializableComment | null> {
        return this.cancelVoteFor(loc, commentId, voterSecret, 'down');
    }

    cancelVoteUp(loc: CommentLocation, commentId: string, voterSecret: string): Promise<SerializableComment | null> {
        return this.cancelVoteFor(loc, commentId, voterSecret, 'up');
    }
}

function getDomain(req: Request): string {
    const host = req.headers.get('HOST');
    if (!host || host.length < 1) {
        throw new Error('Domain cannot be null.  This should return a status 400.');
    }
    if (host.indexOf(':') == -1) {
        return host;
    }
    const ret = host.substring(0, host.indexOf(':'));
    return ret;
}

export function buildCommentAPI(
    router: Router,
    backend: CommentStorageService,
    js: Promise<string>,
    jsMap: Promise<string>,
): Router {
    router.get('/v1', (ctx): void => {
        ctx.response.type = 'application/json';
        ctx.response.body = {};
    });

    router.get('/v1/wui.js', async (ctx): Promise<void> => {
        ctx.response.type = 'text/javascript';
        ctx.response.body = await js;
    });

    router.get('/v1/wui.js.map', async (ctx): Promise<void> => {
        ctx.response.type = 'text/javascript';
        ctx.response.body = await jsMap;
    });

    router.get('/v1/:pageId/comments', async (ctx): Promise<void> => {
        const domain = getDomain(ctx.request);
        const pageId = ctx.params.pageId;
        const loc: CommentLocation = { domain, pageId };
        ctx.response.type = 'application/json';
        ctx.response.body = {
            comments: await backend.listComment(loc),
        };
    });

    router.post('/v1/:pageId/comments', async (ctx): Promise<void> => {
        const domain = getDomain(ctx.request);
        const pageId = ctx.params.pageId;
        const loc: CommentLocation = { domain, pageId };
        const payload: CreateCommitPayload = await ctx.request.body.json();

        ctx.response.type = 'application/json';
        ctx.response.body = await backend.createComment(loc, payload);
    });

    router.get('/v1/:pageId/comments/:commentId', async (ctx): Promise<void> => {
        const domain = getDomain(ctx.request);
        const pageId = ctx.params.pageId;
        const loc: CommentLocation = { domain, pageId };
        ctx.response.type = 'application/json';
        ctx.response.body = {
            comments: await backend.getComment(loc, ctx.params.commentId),
        };
    });

    router.post('/v1/:pageId/comments/:commentId', async (ctx): Promise<void> => {
        const domain = getDomain(ctx.request);
        const pageId = ctx.params.pageId;
        const loc: CommentLocation = { domain, pageId };
        const payload = await ctx.request.body.json();
        ctx.response.type = 'application/json';

        if (!payload.secret && payload.secret.length < 40) {
            ctx.response.status = Status.BadRequest;
            return;
        }
        if (payload.action == 'vote' && payload.value == 'up') {
            const ret = await backend.voteUp(
                loc,
                ctx.params.commentId,
                payload.secret,
            );
            ctx.response.status = Status.Created;
            ctx.response.body = ret;
            return;
        }
        if (payload.action == 'vote' && payload.value == 'down') {
            const ret = await backend.voteDown(
                loc,
                ctx.params.commentId,
                payload.secret,
            );
            ctx.response.status = Status.Created;
            ctx.response.body = ret;
            return;
        }
        if (payload.action == 'cancel-vote' && payload.value == 'up') {
            const ret = await backend.cancelVoteUp(
                loc,
                ctx.params.commentId,
                payload.secret,
            );
            ctx.response.status = Status.Created;
            ctx.response.body = ret;
            return;
        }
        if (payload.action == 'cancel-vote' && payload.value == 'down') {
            const ret = await backend.cancelVoteDown(
                loc,
                ctx.params.commentId,
                payload.secret,
            );
            ctx.response.status = Status.Created;
            ctx.response.body = ret;
            return;
        }
    });

    router.delete('/v1/:pageId/comments/:commentId', async (ctx): Promise<void> => {
        const domain = getDomain(ctx.request);
        const pageId = ctx.params.pageId;
        const loc: CommentLocation = { domain, pageId };
        const payload: { secret: string } = await ctx.request.body.json();
        ctx.response.type = 'application/json';
        ctx.response.body = await backend.deleteComment(
            loc,
            ctx.params.commentId,
            payload.secret,
        );
    });

    return router;
}
