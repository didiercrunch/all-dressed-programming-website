
import duckdb
import json
import psycopg2
from duckdb import DuckDBPyConnection
from collections import deque
from datetime import datetime
from urllib.parse import urlparse, unquote
from contextlib import closing
from typing import Tuple, Optional
from pathlib import PurePosixPath
import pytest
import click

def get_in(d, key):
    if not d:
        return ''
    if isinstance(key, str):
        return d.get(key, '')
    if not key:
        return d
    key = deque(key)
    return get_in(d.get(key.popleft(), ''), key)    


def init_db(conn: DuckDBPyConnection) -> None:
    conn.execute('''
    CREATE TABLE IF NOT EXISTS statistics(
        id                          LONG NOT NULL,
        created_at                  TIMESTAMP NOT NULL,
        canvas_finger_print         VARCHAR(100),
        local_storage_spy_key       VARCHAR(100),
        request_domain              VARCHAR(100) NOT NULL,
        request_path_parts          VARCHAR(100)[] NOT NULL,
        request_headers_referer     TEXT,
        request_headers_user_agent  TEXT,
        sizes_window_inner_width    LONG,
        sizes_window_inner_height   LONG,
    )
    ''')

ParsedReferer = tuple[str, list[str]]

def _split_request_referer(referer: Optional[str]) -> ParsedReferer:
    parsed_url = urlparse(referer or '')
    return (parsed_url.hostname or '',
            [unquote(p) for p in PurePosixPath(parsed_url.path or '/').parts])

def insert_datum_in_db(conn: DuckDBPyConnection,
                       id_: int,
                       created_at: datetime,
                       datum: any) -> None:
    referer = get_in(datum, 'request/headers/referer'.split('/')) or ''
    params = [id_, 
              created_at,
              get_in(datum, 'canvasFingerPrint'),
              get_in(datum, 'localStorageSpyKey'),
              referer,
              get_in(datum, 'request/headers/user-agent'.split('/')),
              get_in(datum, 'sizes/window_innerWidth'.split('/')) or None,
              get_in(datum, 'sizes/window_innerHeight'.split('/')) or None,
              *_split_request_referer(referer)]
    conn.execute('''
    INSERT INTO statistics(id, 
                           created_at,
                           canvas_finger_print, 
                           local_storage_spy_key,
                           request_headers_referer,
                           request_headers_user_agent,
                           sizes_window_inner_width,
                           sizes_window_inner_height,
                           request_domain,
                           request_path_parts)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
    ''', params)

def extract(source_conn, target_conn) -> None:
    min_id = (target_conn.execute('''SELECT max(id) FROM statistics''').fetchone())[0] or 0
    with closing(source_conn.cursor()) as cur:
        cur.execute('''SELECT id, raw_data, created_at 
                       FROM blog_statistics 
                       WHERE id > %s
                       ORDER BY id ASC''', [min_id])
        for id_, raw_data, created_at in cur:
            insert_datum_in_db(target_conn, id_, created_at, raw_data)


@click.command()
@click.option('--target-file', type=click.Path(dir_okay=False, writable=True), required=True)
@click.option('--source-url', type=str, required=True)
def main(target_file, source_url) -> None:
    with psycopg2.connect(source_url) as source_conn:
        with duckdb.connect(target_file) as target_conn:
            init_db(target_conn)
            extract(source_conn, target_conn)
    

@pytest.fixture()
def conn():
    with duckdb.connect() as conn:
        init_db(conn)
        yield conn


def test_get_in():
    assert get_in({}, 'doo') == ''
    assert get_in(None, 'doo') == ''
    assert get_in({'doo': 90}, 'doo') == 90
    assert get_in({'doo': {'foo': 90}}, 'doo') == {'foo': 90}
    assert get_in({'doo': {'foo': 90}}, ['doo', 'foo']) == 90


def test_insert_datum_in_db(conn: DuckDBPyConnection):
    datum = json.loads('''
    {
    "sizes": {
        "window_innerWidth": 1024,
        "window_outerWidth": 1024,
        "window_innerHeight": 1024,
        "window_outerHeight": 1024
    },
    "request": {
        "ip": "34.220.120.11",
        "ips": [],
        "url": "https://all-dressed-programming.com/api/v1/spy",
        "date": 1679930520838,
        "method": "POST",
        "headers": {
            "host": "all-dressed-programming.com",
            "accept": "application/json",
            "origin": "https://all-dressed-programming.com",
            "referer": "https://all-dressed-programming.com/",
            "sec-ch-ua": "",
            "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36",
            "content-type": "application/json",
            "content-length": "982",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "accept-encoding": "gzip, deflate, br",
            "accept-language": "en-US",
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": ""
        }
    },
    "canvasFingerPrint": "qil7MhoE226AhL1VJRVN7AVmHbA=",
    "windowPerformance": {
        "timing": {
            "connectEnd": 1679930516407,
            "domLoading": 1679930517871,
            "fetchStart": 1679930516074,
            "domComplete": 1679930520300,
            "redirectEnd": 0,
            "responseEnd": 1679930517841,
            "connectStart": 1679930516074,
            "loadEventEnd": 1679930520300,
            "requestStart": 1679930516408,
            "redirectStart": 0,
            "responseStart": 1679930517840,
            "domInteractive": 1679930519502,
            "loadEventStart": 1679930520300,
            "unloadEventEnd": 0,
            "domainLookupEnd": 1679930516074,
            "navigationStart": 1679930515935,
            "unloadEventStart": 0,
            "domainLookupStart": 1679930516074,
            "secureConnectionStart": 1679930516174,
            "domContentLoadedEventEnd": 1679930519518,
            "domContentLoadedEventStart": 1679930519502
        },
        "navigation": {
            "type": 0,
            "redirectCount": 0
        },
        "timeOrigin": 1679930515935.9
    },
    "localStorageSpyKey": "3zCDLaAnM7F8uzWhazpsSeHNBX8DEKzMvMPXQJrxEe2tU4tAA7esmjC71ZHqFFVyrg7xq2X7tFP9Q9tq"
}
    ''')
    dt = datetime(2022, 2, 3, 10, 28, 2)
    insert_datum_in_db(conn, 3, dt, datum)
    
    (len_, ) = conn.execute('SELECT COUNT(*) FROM statistics').fetchone()
    assert len_ == 1

@pytest.mark.parametrize(
    "url,result",
    [
        pytest.param('',  ('', ['/'])),
        pytest.param(None,  ('', ['/'])),
        pytest.param('http://example.com',  ('example.com', ['/'])),
        pytest.param('http://example.com/',  ('example.com', ['/'])),
        pytest.param('http://example.com/%7Eguido/bar?tot=12',  ('example.com', ['/', '~guido', 'bar']))
    ],
)
def test_split_request_referer(url, result):
    assert _split_request_referer(url) == result



if __name__ == '__main__':
    main()


